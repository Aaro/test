import os
from fnmatch import fnmatch
import cv2 as cv
from math import pi
import dlib
import csv

dataset = {
    'images': [],
    'categories': [{'id': 0, 'name': 'face'}],
    'annotations': []
}

models = {
    'caffe_model': "models/res10_300x300_ssd_iter_140000_fp16.caffemodel",
    'caffe_configfile': "models/deploy.prototxt",

    'cnn_model': "models/mmod_human_face_detector.dat",

    'dnn_model': "models/opencv_face_detector_uint8.pb",
    'dnn_configFile': "models/opencv_face_detector.pbtxt"
}

input_data = {
    'file_name': "input_data/FDDB-fold-05-ellipseList.txt",
    'images_path': "input_data/original_pics/"
}


def read_dataset():
    file_ellipse_name = input_data['file_name']
    f_annotations = open(file_ellipse_name, 'r')
    list_annotations = f_annotations.read()
    lines = list_annotations.split('\n')
    line_id = 0
    while line_id < len(lines):
        # Image
        img_path = lines[line_id]
        line_id += 1
        image_id = add_image(input_data['images_path'] + img_path + '.jpg')
        img = cv.imread(img_path + '.jpg')
        # Faces
        num_faces = int(lines[line_id])
        line_id += 1
        for i in range(num_faces):
            params = [float(v) for v in lines[line_id].split()]
            line_id += 1
            left, top, right, bottom = ellipse_to_rect(params)
            add_bbox(image_id, left, top, width=right - left + 1, height=bottom - top + 1)


def add_image(image_path):
    assert ('images' in dataset)
    image_id = len(dataset['images'])
    dataset['images'].append({
        'id': int(image_id),
        'file_name': image_path
    })
    return image_id


def ellipse_to_rect(params):
    rad_x = params[0]
    rad_y = params[1]
    angle = params[2] * 180.0 / pi
    center_x = params[3]
    center_y = params[4]
    pts = cv.ellipse2Poly((int(center_x), int(center_y)), (int(rad_x), int(rad_y)), int(angle), 0, 360, 10)
    rect = cv.boundingRect(pts)
    left = rect[0]
    top = rect[1]
    right = rect[0] + rect[2]
    bottom = rect[1] + rect[3]
    return left, top, right, bottom


def add_bbox(image_id, left, top, width, height):
    assert ('annotations' in dataset)
    dataset['annotations'].append({
        'id': len(dataset['annotations']),
        'image_id': int(image_id),
        'category_id': 0,  # Face
        'bbox': [int(left), int(top), int(width), int(height)],
        'iscrowd': 0,
        'area': float(width * height)
    })


def add_detection(detections, image_id, left, top, width, height, score):
    detections.append({
        'image_id': int(image_id),
        'category_id': 0,  # Face
        'bbox': [int(left), int(top), int(width), int(height)],
        'score': float(score)
    })


def fddb_dataset(annotations, images):
    for d in os.listdir(annotations):
        if fnmatch(d, input_data['file_name']):
            with open(os.path.join(annotations, d), 'rt') as f:
                lines = [line.rstrip('\n') for line in f]
                line_id = 0
                while line_id < len(lines):
                    # Image
                    img_path = lines[line_id]
                    line_id += 1
                    image_id = add_image(os.path.join(images, img_path) + '.jpg')
                    # Faces
                    num_faces = int(lines[line_id])
                    line_id += 1
                    for i in range(num_faces):
                        params = [float(v) for v in lines[line_id].split()]
                        line_id += 1
                        left, top, right, bottom = ellipse_to_rect(params)
                        add_bbox(image_id, left, top, width=right - left + 1, height=bottom - top + 1)


def dnn_caffe_face_detector_in_opencv(image_path):
    model_file = models['caffe_model']
    config_file = models['caffe_configfile']
    net = cv.dnn.readNetFromCaffe(config_file, model_file)
    img = cv.imread(image_path)
    frame_height, frame_width = img.shape[:2]
    blob = cv.dnn.blobFromImage(img, 1.0, (300, 300), [104, 117, 123], False, False)  # cv.dnn.blobFromImage(img)
    net.setInput(blob)
    detections = net.forward()
    boxes = []
    scores = []
    conf_threshold = 0.6
    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > conf_threshold:
            x1 = int(detections[0, 0, i, 3] * frame_width)
            y1 = int(detections[0, 0, i, 4] * frame_height)
            x2 = int(detections[0, 0, i, 5] * frame_width)
            y2 = int(detections[0, 0, i, 6] * frame_height)
            boxes.append([x1, y1, x2, y2])
            scores.append(confidence)
    return boxes, scores


def dnn_face_detector_in_opencv(image_path):
    model_file = models['dnn_model']
    config_file = models['dnn_configFile']
    net = cv.dnn.readNetFromTensorflow(model_file, config_file)
    img = cv.imread(image_path)
    frame_height, frame_width = img.shape[:2]
    blob = cv.dnn.blobFromImage(img, 1.0, (300, 300), [104, 117, 123], False, False)
    net.setInput(blob)
    detections = net.forward()
    boxes = []
    scores = []
    conf_threshold = 0.6
    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > conf_threshold:
            x1 = int(detections[0, 0, i, 3] * frame_width)
            y1 = int(detections[0, 0, i, 4] * frame_height)
            x2 = int(detections[0, 0, i, 5] * frame_width)
            y2 = int(detections[0, 0, i, 6] * frame_height)
            boxes.append([x1, y1, x2, y2])
            scores.append(confidence)
    return boxes, scores


def cnn_face_detector_in_dlib(image_path):
    img = dlib.load_rgb_image(image_path)
    cnn_face_detector = dlib.cnn_face_detection_model_v1(models['cnn_model'])
    face_rects = cnn_face_detector(img, 1)
    scores = []
    boxes = []
    for face_rect in face_rects:
        x1 = face_rect.rect.left()
        y1 = face_rect.rect.top()
        x2 = face_rect.rect.right()
        y2 = face_rect.rect.bottom()
        boxes.append([x1, y1, x2, y2])
        scores.append(1.0)
    return boxes, scores


def iou(rect_1, rect_2):
    x11 = rect_1[0]  # first rectangle top left x
    y11 = rect_1[1]  # first rectangle top left y
    x12 = rect_1[2]  # first rectangle bottom right x
    y12 = rect_1[3]  # first rectangle bottom right y

    x21 = rect_2[0]  # second rectangle top left x
    y21 = rect_2[1]  # second rectangle top left y
    x22 = x21 + rect_2[2]  # second rectangle bottom right x
    y22 = y21 + rect_2[3]  # second rectangle bottom right y
    x_overlap = max(0, min(x12, x22) - max(x11, x21))
    y_overlap = max(0, min(y12, y22) - max(y11, y21))
    intersection = x_overlap * y_overlap
    union = (x12 - x11) * (y12 - y11) + (x22 - x21) * (y22 - y21) - intersection
    if union > 0:
        return float(intersection) / union
    else:
        return 0


def solve_ap(true_boxes, solve_boxes, scores):
    tp = 0
    fp = 0
    solve_list = []
    for box, score in zip(solve_boxes, scores):
        solve_list.append({'box': box, 'score': score, 'tp': False})

    for true_box in true_boxes:
        boxes_list = []
        scores_list = []
        for box in solve_list:
            if not box['tp']:
                if iou(box['box'], true_box) > 0.5:
                    boxes_list.append(box['box'])
                    scores_list.append(box['score'])
                    box['tp'] = True
        if len(boxes_list) > 0:
            # cv.dnn.NMSBoxes(boxes_list, scores_list, 0.5, 0.3)
            tp = tp + 1
    for box in solve_list:
        if not box['tp']:
            fp = fp + 1
    ap = tp / (tp + fp)
    return ap


def solve_map():
    dnn_sum_ap = 0
    cnn_sum_ap = 0
    caffe_sum_ap = 0

    # f1 = open('cnn_model.txt', 'w')
    # wr1 = csv.writer(f1)
    # f2 = open('caffe_model.txt', 'w')
    # wr2 = csv.writer(f2)

    images = dataset['images']

    for img in images:  # для всех изображений в наборе
        true_boxes = get_true_boxes(img)
        caffe_solve_boxes, scores = dnn_caffe_face_detector_in_opencv(img['file_name'])
        caffe_sum_ap = caffe_sum_ap + solve_ap(true_boxes, caffe_solve_boxes, scores)
        # f2.write(str(len(caffe_solve_boxes)) + "\n")
        # wr2.writerows(caffe_solve_boxes)
    caffe_map = caffe_sum_ap / len(images)
    print(caffe_map)

    for img in images:
        true_boxes = get_true_boxes(img)
        dnn_solve_boxes, scores = dnn_face_detector_in_opencv(img['file_name'])
        dnn_sum_ap = dnn_sum_ap + solve_ap(true_boxes, dnn_solve_boxes, scores)
    dnn_map = dnn_sum_ap / len(images)
    print(dnn_map)

    for img in images:
        true_boxes = get_true_boxes(img)
        cnn_solve_boxes, scores = cnn_face_detector_in_dlib(img['file_name'])
        cnn_sum_ap = cnn_sum_ap + solve_ap(true_boxes, cnn_solve_boxes, scores)
        # f1.write(str(len(cnn_solve_boxes)) + "\n")
        # wr1.writerows(cnn_solve_boxes)
    cnn_map = cnn_sum_ap / len(images)
    print(cnn_map)


def get_true_boxes(img):
    annotations = dataset['annotations']
    index = img['id']
    true_boxes = []
    true_info = list(filter(lambda annotation: annotation['image_id'] == index, annotations))
    for info in true_info:
        true_boxes.append(info['bbox'])
    return true_boxes


######################


read_dataset()
solve_map()
